import * as chalk from 'chalk'

export function main() {
  const result = 7

  console.log(chalk.green(JSON.stringify(result)))

  return result
}
