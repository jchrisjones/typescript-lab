import { main } from '.'

describe('main', () => {
  it('should work', () => {
    const expected = 7
    const result = main()

    expect(result).toStrictEqual(expected)
  })
})
